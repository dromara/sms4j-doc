import {defineUserConfig} from "vuepress";
import {hopeTheme} from "vuepress-theme-hope";
import { getDirname, path } from "@vuepress/utils";

const __dirname = getDirname(import.meta.url);

// @ts-ignore
export default defineUserConfig({
    head:[['script',{},
    'var _hmt = _hmt || [];\n' +
    '(function() {\n' +
    '  var hm = document.createElement("script");\n' +
    '  hm.src = "https://hm.baidu.com/hm.js?804a18ac6ef19f32a21420e83e5886cc";\n' +
    '  var s = document.getElementsByTagName("script")[0]; \n' +
    '  s.parentNode.insertBefore(hm, s);\n' +
    '})();\n'
    ],
    [
        "meta",
        {
          name: "wwads-cn-verify",
          content: "310dcbbf4cce62f762a2aaa148d556bd",
        },
      ],
      ["script", { src: "https://cdn.wwads.cn/js/makemoney.js" }],
      [
        "script",
        {},
        `// function called if wwads is blocked
        function ABDetected() {
          document.getElementsByClassName("wwads-cn")[0].insertAdjacentHTML("beforeend", "<style>.wwads-horizontal,.wwads-vertical{background-color:#f4f8fa;padding:5px;min-height:120px;margin-top:20px;box-sizing:border-box;border-radius:3px;font-family:sans-serif;display:flex;min-width:150px;position:relative;overflow:hidden;}.wwads-horizontal{flex-wrap:wrap;justify-content:center}.wwads-vertical{flex-direction:column;align-items:center;padding-bottom:32px}.wwads-horizontal a,.wwads-vertical a{text-decoration:none}.wwads-horizontal .wwads-img,.wwads-vertical .wwads-img{margin:5px}.wwads-horizontal .wwads-content,.wwads-vertical .wwads-content{margin:5px}.wwads-horizontal .wwads-content{flex:130px}.wwads-vertical .wwads-content{margin-top:10px}.wwads-horizontal .wwads-text,.wwads-content .wwads-text{font-size:14px;line-height:1.4;color:#0e1011;-webkit-font-smoothing:antialiased}.wwads-horizontal .wwads-poweredby,.wwads-vertical .wwads-poweredby{display:block;font-size:11px;color:#a6b7bf;margin-top:1em}.wwads-vertical .wwads-poweredby{position:absolute;left:10px;bottom:10px}.wwads-horizontal .wwads-poweredby span,.wwads-vertical .wwads-poweredby span{transition:all 0.2s ease-in-out;margin-left:-1em}.wwads-horizontal .wwads-poweredby span:first-child,.wwads-vertical .wwads-poweredby span:first-child{opacity:0}.wwads-horizontal:hover .wwads-poweredby span,.wwads-vertical:hover .wwads-poweredby span{opacity:1;margin-left:0}.wwads-horizontal .wwads-hide,.wwads-vertical .wwads-hide{position:absolute;right:-23px;bottom:-23px;width:46px;height:46px;border-radius:23px;transition:all 0.3s ease-in-out;cursor:pointer;}.wwads-horizontal .wwads-hide:hover,.wwads-vertical .wwads-hide:hover{background:rgb(0 0 0 /0.05)}.wwads-horizontal .wwads-hide svg,.wwads-vertical .wwads-hide svg{position:absolute;left:10px;top:10px;fill:#a6b7bf}.wwads-horizontal .wwads-hide:hover svg,.wwads-vertical .wwads-hide:hover svg{fill:#3E4546}</style><a href='https://wwads.cn/page/whitelist-wwads' class='wwads-img' target='_blank' rel='nofollow'><img src='https://creatives-1301677708.file.myqcloud.com/images/placeholder/wwads-friendly-ads.png' width='130'></a><div class='wwads-content'><a href='https://wwads.cn/page/whitelist-wwads' class='wwads-text' target='_blank' rel='nofollow'>为了本站的长期运营，请将我们的网站加入广告拦截器的白名单，感谢您的支持！</a><a href='https://wwads.cn/page/end-user-privacy' class='wwads-poweredby' title='万维广告 ～ 让广告更优雅，且有用' target='_blank'><span>万维</span><span>广告</span></a></div><a class='wwads-hide' onclick='parentNode.remove()' title='隐藏广告'><svg xmlns='http://www.w3.org/2000/svg' width='6' height='7'><path d='M.879.672L3 2.793 5.121.672a.5.5 0 11.707.707L3.708 3.5l2.12 2.121a.5.5 0 11-.707.707l-2.12-2.12-2.122 2.12a.5.5 0 11-.707-.707l2.121-2.12L.172 1.378A.5.5 0 01.879.672z'></path></svg></a>");
        };
        
        //check document ready
        function docReady(t) {
            "complete" === document.readyState ||
            "interactive" === document.readyState
              ? setTimeout(t, 1)
              : document.addEventListener("DOMContentLoaded", t);
        }
        
        //check if wwads' fire function was blocked after document is ready with 3s timeout (waiting the ad loading)
        docReady(function () {
          setTimeout(function () {
            if( window._AdBlockInit === undefined ){
                ABDetected();
            }
          }, 3000);
        });`,
      ],
    ],
    base: "/",
    lang: "zh-CN",
    locales: {
        "/": {
            lang: "zh-CN",
            title: "SMS4J",
            description: "SMS4J文档",
        },
    },
    theme: hopeTheme({
        logo:"/logo.png",
        iconAssets: "//at.alicdn.com/t/c/font_3977841_tfbu1j5yn.js",
        themeColor: {
            blue: "#2196f3",
            red: "#f26d6d",
            green: "#3eaf7c",
            orange: "#fb9b5f",
        },
        navbar: [
            {
                text: "🏡首页",
                link: "/README.md",
                icon: "lightbulb",
                // // 仅在 `/zh/guide/` 激活
                // activeMatch: "^/zh/guide/$",
            },
            {text: "📖文档",
                children: [
                    {
                        text: "🎈2.0文档",
                        link: "/doc/doc2/README.md",
                    },
                    {
                        text: "💎3.0文档",
                        link: "/doc/doc3/README.md",
                    },
                ],
                icon: "config"},
            {text: "📒javaDoc", link: "https://apidoc.gitee.com/dromara/sms4j", icon: "config"},
            {text: "🤝Dromara组织", link: "https://dromara.org/zh/", icon: "config"},
            {text: "💡项目历程",
                children:[
                    {
                        text: "⏳更新日志",
                        link: "/doc/doc2/log.md",
                    },
                    {
                        text: "🎎开发组成员",
                        link: "/doc/developer.md",
                    },
                ],
                icon: "config"},
            {text: "💖赞助列表", link: "/doc/sponsor.md", icon: "config"},
            {text: "👪加入交流群", link: "/doc/doc2/group.md", icon: "config"},
            {text: "🔍常见问题", link: "/doc/doc2/issue.md", icon: "config"},
            {text: "🏮仓库地址",
                children: [
                    {
                        text: "🍎gitee",
                        link: "https://gitee.com/dromara/sms4j",
                    },
                    {
                        text: "🍏github",
                        link: "https://github.com/dromara/SMSAggregation",
                    },
                ],
                icon: "config"},

        ],
        sidebar: {
            "/doc/doc2/": [
                "" /* /foo/index.html */,
                "/doc/doc2/springboot" /* /foo/geo.html */,
                "/doc/doc2/solon",
                "/doc/doc2/javase",
                "/doc/doc2/jinjiepeizhi",
                "sql/sql",
                "sql/custom",
                "supplier/jieshao",
                "supplier/aliyun" /* /foo/geo.html */,
                "supplier/unisms",
                "supplier/tencent",
                "supplier/yunpian",
                "supplier/huawei",
                "supplier/jdcloud",
                "supplier/cloopen",
                "supplier/emay",
                "supplier/ctyun",
                "supplier/wangyi",
                "api/api.md" /* /foo/index.html */,
                "api/tool.md" /* /foo/index.html */,
                "plugin/mail.md" /* /foo/index.html */,
            ],
},
        plugins:{
            components:{
                rootComponents:{
                    notice:[
                        {
                            path:"/",
                            title:"📢公告",
                            content:"🎉SMS4J 2.2.0正式发布",
                            actions:[
                                {
                                    text:"gitee",
                                    link:"https://gitee.com/dromara/sms4j",
                                    type:"primary"
                                },
                                {
                                    text:"github",
                                    link:"https://github.com/fengruge/sms_aggregation",
                                    type:"default"
                                }
                            ]
                        },
                        {
                            path:"/doc/doc2/",
                            title:"📫征集令",
                            content:"🎀🎀在这里我们面向全员征集参与者和使用者🎀🎀如果你的公司或项目正在使用我们的工具，请在lssues中告诉我们，我们将会在征求您的同意后展示在官网",
                            actions:[
                                {
                                    text:"lssues",
                                    link:"https://gitee.com/dromara/sms4j/issues",
                                    type:"primary"
                                },
                            ]
                        }
                    ]
                }
            },
            mdEnhance:{
                mermaid: true,
                card: true
            }
        }

    }),
    shouldPrefetch: false,
    alias: {
        "@theme-hope/components/HomePage": path.resolve(
            __dirname,
            "./components/HomePage.vue"
        ),
        "@theme-hope/components/NormalPage": path.resolve(
            __dirname,
            "./components/NormalPage.vue"
        ),
    }
});
