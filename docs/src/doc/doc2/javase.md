---
# 这是文章的标题
title: 🍵在JavaSE环境集成
# 这是页面的图标
icon: <svg t="1679837826543" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1201" width="200" height="200"><path d="M763.136 958.72H262.656c-50.944 0-92.16-41.216-92.16-92.16V518.144H107.776c-25.6 0-48.128-15.36-57.6-39.168s-3.584-50.432 14.848-68.096c0.256-0.256 0.512-0.512 0.768-0.512l344.064-307.2c56.576-53.248 145.408-53.76 202.496-1.28l346.624 307.2 0.512 0.512c18.944 17.408 25.088 44.288 15.616 68.352-9.472 24.064-32 39.424-57.856 39.424h-61.696v348.928c-0.256 50.944-41.472 92.416-92.416 92.416zM107.52 456.704h93.696c16.896 0 30.72 13.824 30.72 30.72v379.136c0 16.896 13.824 30.72 30.72 30.72h500.48c16.896 0 30.72-13.824 30.72-30.72V486.656c0-16.896 13.824-30.72 30.72-30.72H917.504s0.256-0.512 0.256-0.768l-0.256-0.256-346.368-307.2-0.512-0.512c-33.536-30.976-86.016-30.72-119.04 0.768-0.256 0.256-0.512 0.512-0.768 0.512L107.264 455.68c0 0.256-0.256 0.256-0.256 0.256s0.256 0.512 0.512 0.768c-0.256 0 0 0 0 0z m0 0z" fill="#040000" p-id="1202"></path><path d="M644.608 897.024h-61.44v-218.112c0-16.64-13.824-29.952-30.72-29.952H471.04c-16.896 0-30.72 13.568-30.72 29.952v218.112h-61.44v-218.112c0-50.432 41.216-91.392 92.16-91.392h81.408c50.944 0 92.16 40.96 92.16 91.392v218.112z" fill="#D63123" p-id="1203"></path></svg>
# 这是侧边栏的顺序
order: 3
# 设置作者
author: xiaoyan
# 设置写作时间
date: 2023-06-15
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在文章收藏中
star: true
# 你可以自定义页脚
footer: © 2022 wind <a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank">冀ICP备2021004949号-3</a>
# 你可以自定义版权信息
# copyright: 无版权
---

## 在JavaSE环境集成
### 1.创建项目
在 IDE 中新建一个 Java 项目，例如：sms-demo-javase

### 2.添加依赖
在项目中添加maven依赖：

```xml
<dependency>
        <groupId>org.dromara.sms4j</groupId>
        <artifactId>sms4j-javase-plugin</artifactId>
        <version> version </version>
</dependency>
```

### 3.初始化短信配置
在调用短信发送之前进行短信配置，多次发送只配置一次即可。当前支持4种配置方式，可根据需要选择一种进行配置。具体配置项可参考各厂商差异化配置和进阶配置等内容。

* 方式1：使用yaml配置文件

在资源目录中创建配置文件sms4j.yml，然后写入相关配置，书写方式同springboot的yaml配置
```yaml
sms:
  alibaba:
    access-key-id: 您的accessKey
    access-key-secret: 您的accessKeySecret
    template-id: 您的templateId
    template-name: 您的templateName
    signature: 您的短信签名
# 其他配置……
```
调用配置方法。fromYaml()方法会读取并加载sms4j.yml中的配置

```java
SEInitializer.initializer().fromYaml();
```

* 方式2：使用yaml配置字符串

调用fromYaml(String)，直接传入yaml配置字符串

```java
String yaml = "sms:\n" +
        "  alibaba:\n" +
        "    access-key-id: 您的accessKey\n" +
        "    access-key-secret: 您的accessKeySecret\n" +
        "    template-id: 您的templateId\n" +
        "    template-name: 您的templateName\n" +
        "    signature: 您的短信签名";
SEInitializer.initializer().fromYaml(yaml);
```

* 方式3：使用json配置字符串

```java
String json = "{\n" +
        "    \"sms\": {\n" +
        "        \"alibaba\": {\n" +
        "            \"templateName\": \"您的templateName\",\n" +
        "            \"accessKeyId\": \"您的accessKey\",\n" +
        "            \"accessKeySecret\": \"您的accessKeySecret\",\n" +
        "            \"signature\": \"您的短信签名\",\n" +
        "            \"templateId\": \"您的templateId\"\n" +
        "        }\n" +
        "    }\n" +
        "}";
SEInitializer.initializer().fromJson(json);
```

* 方式4：自定义配置

通过各厂商构造器实例化配置实例，然后初始化

```java
// 实例化各厂商配置
SmsConfig smsConfig = new SmsConfig();
smsConfig.setConfigType(ConfigType.SETTINGS_FILE);
AlibabaConfig alibabaConfig = AlibabaConfig.builder()
        .accessKeyId("您的accessKey")
        .accessKeySecret("您的accessKeySecret")
        .templateId("您的templateId")
        .templateName("您的templateName")
        .signature("您的短信签名")
        .build();
TencentConfig tencentConfig = TencentConfig.builder()
        .accessKeyId("您的accessKey")
        .accessKeySecret("您的accessKeySecret")
        .signature("您的短信签名")
        .templateId("您的templateId")
        .sdkAppId("短信sdkAppId")
        .build();
// 初始化配置
SEInitializer.initializer()
        .initSmsConfig(smsConfig)
        .initAlibaba(alibabaConfig)
        .initTencent(tencentConfig);
```

### 4.发送短信

调用发送方法发送短信

```java
SmsFactory.createSmsBlend(SupplierType.ALIBABA).sendMessage("手机号码", "短信");
```
