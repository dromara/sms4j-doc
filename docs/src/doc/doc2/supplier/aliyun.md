---
# 这是文章的标题
title: 🎷阿里云短信
# 这是页面的图标
icon: <svg t="1679837826543" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1201" width="200" height="200"><path d="M763.136 958.72H262.656c-50.944 0-92.16-41.216-92.16-92.16V518.144H107.776c-25.6 0-48.128-15.36-57.6-39.168s-3.584-50.432 14.848-68.096c0.256-0.256 0.512-0.512 0.768-0.512l344.064-307.2c56.576-53.248 145.408-53.76 202.496-1.28l346.624 307.2 0.512 0.512c18.944 17.408 25.088 44.288 15.616 68.352-9.472 24.064-32 39.424-57.856 39.424h-61.696v348.928c-0.256 50.944-41.472 92.416-92.416 92.416zM107.52 456.704h93.696c16.896 0 30.72 13.824 30.72 30.72v379.136c0 16.896 13.824 30.72 30.72 30.72h500.48c16.896 0 30.72-13.824 30.72-30.72V486.656c0-16.896 13.824-30.72 30.72-30.72H917.504s0.256-0.512 0.256-0.768l-0.256-0.256-346.368-307.2-0.512-0.512c-33.536-30.976-86.016-30.72-119.04 0.768-0.256 0.256-0.512 0.512-0.768 0.512L107.264 455.68c0 0.256-0.256 0.256-0.256 0.256s0.256 0.512 0.512 0.768c-0.256 0 0 0 0 0z m0 0z" fill="#040000" p-id="1202"></path><path d="M644.608 897.024h-61.44v-218.112c0-16.64-13.824-29.952-30.72-29.952H471.04c-16.896 0-30.72 13.568-30.72 29.952v218.112h-61.44v-218.112c0-50.432 41.216-91.392 92.16-91.392h81.408c50.944 0 92.16 40.96 92.16 91.392v218.112z" fill="#D63123" p-id="1203"></path></svg>
# 这是侧边栏的顺序
order: 5
# 设置作者
author: wind
# 设置写作时间
date: 2023-03-27
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在文章收藏中
star: true
# 你可以自定义页脚
footer: © 2022 wind <a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank">冀ICP备2021004949号-3</a>
# 你可以自定义版权信息
# copyright: 无版权
---

### 厂商信息

[产品计费](https://help.aliyun.com/document_detail/44350.htm?spm=a2c4g.44340.0.0.3d451ed8sNEOBm#topic1201)

### 基础配置

```yaml
sms:
  alibaba:
    #阿里云的accessKey
    accessKeyId: 您的accessKey
    #阿里云的accessKeySecret
    accessKeySecret: 您的accessKeySecret
    #短信签名
    signature: 测试签名
    #模板ID 用于发送固定模板短信使用
    templateId: SMS_215125134
    #模板变量 上述模板的变量
    templateName: code
    #请求地址默认为 dysmsapi.aliyuncs.com 如无特殊改变可以不用设置
    requestUrl: dysmsapi.aliyuncs.com
    #接口方法默认为 SendSms 如无特殊改变可以不用设置
    action: SendSms
    #接口版本号默认为 2017-05-25 如无特殊改变可以不用设置
    version: 2017-05-25
    #地域信息默认为 cn-hangzhou 如无特殊改变可以不用设置
    regionId: cn-hangzhou
```
### 数据库风格配置
```json
{
  "accessKeyId": "您的accessKey",
  "accessKeySecret": "您的accessKeySecret",
  "signature": "测试签名",
  "templateId":"SMS_215125134",
  "templateName": "code",
  "requestUrl": "dysmsapi.aliyuncs.com",
  "action": "SendSms",
  "version": "2017-05-25",
  "regionId": "cn-hangzhou"
}
```
### 手动写入配置文件风格
```java
@Configuration
public class AliConfiguration{

    @Bean
    public void setConfiguration(){
        AlibabaConfig alibabaConfig = SupplierFactory.getAlibabaConfig();
        alibabaConfig.setAccessKeyId("您的accessKey");
        alibabaConfig.setAccessKeySecret("您的accessKeySecret");
        alibabaConfig.setSignature("测试签名");
        alibabaConfig.setTemplateId("SM123581321");
        alibabaConfig.setTemplateName("code");
        alibabaConfig.setAction("SendSms");
        alibabaConfig.setVersion("2017-05-25");
        alibabaConfig.setRegionId("cn-hangzhou");

    }
}
```
### 其他方式
如果你想在某个环节动态的改变配置中的值，可以随时通过`SupplierFactory.getAlibabaConfig()`获取阿里云的单例配置对象，并且修改它的值，但是要注意的是，
如果你修改了阿里云配置的值在发送短信前必须至少调用一次`SmsFactory.refresh(SupplierType.ALIBABA);`方法进行配置刷新。