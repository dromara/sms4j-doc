---
title: 🥝支持厂商
index: false
icon: play
---
<h2 align="center" style="margin: 0 0 0px; font-weight: bold;">🎁支持厂商一览</h2>

_自2.0.0版本开始，配置文件中可以同时配置多个厂商，以启用多个厂商的短信_
- [🚁阿里云国内短信](aliyun.html)
- [🚀腾讯云国内短信](tencent.html)
- [🛸华为云国内短信](huawei.html)
- [🐶京东云国内短信](jdcloud.html)
- [🎡容联云国内短信](cloopen.html)
- [🚠亿美软通短信](emay.html)
- [🛳️网易云短信](wangyi.html)
- [🛸天翼云短信](ctyun.html)
- [🚅合一短信](unisms.html)
- [🛰️云片短信](yunpian.html)
