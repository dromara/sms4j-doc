---
# 这是文章的标题
title: 🛳️网易云短信
# 这是页面的图标
icon: 
# 这是侧边栏的顺序
order: 7
# 设置作者
author: wind
# 设置写作时间
date: 2023-06-15
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在文章收藏中
star: true
# 你可以自定义页脚
footer: © 2022 wind <a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank">冀ICP备2021004949号-3</a>
# 你可以自定义版权信息
# copyright: 无版权
---

### 厂商信息

[产品计费](https://netease.im/price/sms)

### 基础配置

```yaml
sms:
  netease:
    # 访问键标识
    accessKeyId: your accessKeyId
    # 访问键秘钥 
    accessKeySecret: your accessKeySecret
    # 短信签名
    signature: your signature
    # 模板Id 发送固定模板短信时使用的此配置
    templateId: your templateId
    # 模板变量名称 上述模板的变量名称
    templateName: your templateName
    # 模板短信请求地址
    templateUrl: https://api.netease.im/sms/sendtemplate.action
    # 验证码短信请求地址
    codeUrl: https://api.netease.im/sms/sendcode.action
    # 验证码验证请求地址
    verifyUrl: https://api.netease.im/sms/verifycode.action
    # 是否需要支持短信上行。true:需要，false:不需要
    needUp: false
```
### 数据库风格配置
```json
{
  "accessKeyId": "your accessKeyId",
  "accessKeySecret": "your accessKeySecret",
  "signature": "your signature",
  "templateId": "your templateId",
  "templateName": "your templateName",
  "templateUrl": "your templateUrl",
  "codeUrl": "your codeUrl",
  "needUp": false
}
```
### 手动写入配置文件风格
```java
@Configuration
public class SmsConfiguration {
    
    @Bean
    public NeteaseConfig neteaseConfig() {
        NeteaseConfig neteaseConfig = SupplierFactory.getNeteaseConfig();
        neteaseConfig.setAccessKeyId("your accessKeyId");
        neteaseConfig.setAccessKeySecret("your accessKeySecret");
        neteaseConfig.setSignature("your signature");
        neteaseConfig.setTemplateId("your templateId");
        neteaseConfig.setTemplateName("your templateName");
        neteaseConfig.setTemplateUrl("your TemplateUrl");
        neteaseConfig.setCodeUrl("your CodeUrl");
        neteaseConfig.setVerifyUrl("your VerifyUrl");
        return uniConfig;
    }
}
```
### 其他方式
如果你想在某个环节动态的改变配置中的值，可以随时通过
`SupplierFactory.getNeteaseConfig()` 获取合一短信的单例配置对象，并且修改它的值，
如果你修改了合一短信配置的值在发送短信前必须至少调用一次
`SmsFactory.refresh(SupplierType.NETEASE)`;方法进行配置刷新。