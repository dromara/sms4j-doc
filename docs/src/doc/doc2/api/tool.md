---
title: 🔧常用工具
index: false
icon: play
---

在日常的项目中我们发现，会有很多常用的方法需要重复的编写，而且这些方法在常见的工具类中并没有存在，我们在这里整理了一部分，后续将会继续添加
这些工具我们把他放置在 `kim.wind.sms.comm.utils` 包中
`SmsUtil`中集成了多数的静态方法


#### 获取一个指定长度的随机字符串，包含数字和大小写字母，不包含符号和空格
```java
SmsUtil.getRandomString(6)
```
#### 获取一个六位长度的随机字符串，包含数字和大小写字母，不包含符号和空格
```java
SmsUtil.getRandomString()
```
#### 获取一个指定长度的随机纯数字组成的字符串
```java
SmsUtil.getRandomInt(4)
```
#### 获取一个空的LinkedHashMap对象
```java
LinkedHashMap<String, String> getNewMap()
```

在2.0.2版本中，我们添加了一个实验性的功能，基于平滑加权负载均衡算法实现了一个负载均衡器位于`org.dromara.sms4j.core.load`包中  
此工具经改造后已贡献给 Hutool6.0 后续将持续更细完善
#### 添加负载短信实现
```java
//在addLoadServer方法中，接收短信实现和权重，并交托给负载均衡器
SmsBlend sms = SmsFactory.createSmsBlend(SupplierType.ALIBABA);
SmsLoad.addLoadServer(sms,5);
```
#### 获取短信实现,getLoadServer方法会根据算法获取出权重结果
```java
SmsBlend loadServer = SmsLoad.getLoadServer();
```
#### 删除均衡器托管的短信实现
```java
SmsLoad.removeLoadServer(sms);
```

### 自2.1.0版本开始，sms4j的短信限制功能实现方式发生变动，现在不再进行全局的短信拦截，而是可以根据厂商进行针对性的拦截
:::tip
需要注意的是，短信拦截的实现对象可以和标准的对象共存
:::
#### 获取某个厂商带有拦截功能的短信实现
```java
SmsBlend getRestrictedSmsBlend(SupplierType supplierType)
```
#### 刷新带有短信拦截的对象实现
```java
void refreshRestrictedSmsBlend(SupplierType supplierType)
```