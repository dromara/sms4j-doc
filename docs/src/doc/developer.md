---
# 这是文章的标题
title: 💻贡献者列表
# 这是页面的图标
icon: <svg t="1679837826543" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1201" width="200" height="200"><path d="M763.136 958.72H262.656c-50.944 0-92.16-41.216-92.16-92.16V518.144H107.776c-25.6 0-48.128-15.36-57.6-39.168s-3.584-50.432 14.848-68.096c0.256-0.256 0.512-0.512 0.768-0.512l344.064-307.2c56.576-53.248 145.408-53.76 202.496-1.28l346.624 307.2 0.512 0.512c18.944 17.408 25.088 44.288 15.616 68.352-9.472 24.064-32 39.424-57.856 39.424h-61.696v348.928c-0.256 50.944-41.472 92.416-92.416 92.416zM107.52 456.704h93.696c16.896 0 30.72 13.824 30.72 30.72v379.136c0 16.896 13.824 30.72 30.72 30.72h500.48c16.896 0 30.72-13.824 30.72-30.72V486.656c0-16.896 13.824-30.72 30.72-30.72H917.504s0.256-0.512 0.256-0.768l-0.256-0.256-346.368-307.2-0.512-0.512c-33.536-30.976-86.016-30.72-119.04 0.768-0.256 0.256-0.512 0.512-0.768 0.512L107.264 455.68c0 0.256-0.256 0.256-0.256 0.256s0.256 0.512 0.512 0.768c-0.256 0 0 0 0 0z m0 0z" fill="#040000" p-id="1202"></path><path d="M644.608 897.024h-61.44v-218.112c0-16.64-13.824-29.952-30.72-29.952H471.04c-16.896 0-30.72 13.568-30.72 29.952v218.112h-61.44v-218.112c0-50.432 41.216-91.392 92.16-91.392h81.408c50.944 0 92.16 40.96 92.16 91.392v218.112z" fill="#D63123" p-id="1203"></path></svg>
# 这是侧边栏的顺序
order: 1
# 设置作者
author: wind
# 设置写作时间
date: 2023-04-12
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在文章收藏中
star: true
# 你可以自定义页脚
footer: © 2022 wind <a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank">冀ICP备2021004949号-3</a>
# 你可以自定义版权信息
# copyright: 无版权
---


## 开发团队
*在此鸣谢所有的贡献者对于本项目和开源事业做出的伟大贡献*

<div class="developer creator">
<a href="https://gitee.com/MR-wind" target="_blank">
<img class="heardImg" src="/developer/me.jpg">
<p class="nameP">
wind
</p>
</a>
</div>

<div id="devs">
<div class="developer">
<a href="https://blog.charles7c.top/about/me" target="_blank">
<img class="heardImg" src="/developer/charles.png" style="border-radius: 9999px;">
<p class="nameP">
Charles7c
</p>
</a>
</div>

<div class="developer">
<a href="https://gitee.com/m_yan" target="_blank">
<img class="heardImg" src="/developer/heng.jpg" style="border-radius: 9999px;">
<p class="nameP">
heng
</p>
</a>
</div>

<div class="developer">
<a href="https://gitee.com/litairan666" target="_blank">
<img class="heardImg" src="/developer/richard.jpg" style="border-radius: 9999px;">
<p class="nameP">
Richard
</p>
</a>
</div>

<div class="developer">
<a href="https://gitee.com/Bleachtred" target="_blank">
<img class="heardImg" src="/developer/bleachtred.jpg" style="border-radius: 9999px;">
<p class="nameP">
bleachtred
</p>
</a>
</div>

<div class="developer">
<a href="https://gitee.com/handy-git" target="_blank">
<img class="heardImg" src="/developer/handy.jpg" style="border-radius: 9999px;">
<p class="nameP">
handy
</p>
</a>
</div>

<div class="developer">
<a href="https://github.com/TJxiaobao" target="_blank">
<img class="heardImg" src="/developer/tiejiaxiaobao.jpg" style="border-radius: 9999px;">
<p class="nameP">
铁甲小宝
</p>
</a>
</div>

<div class="developer">
<a href="https://www.yuque.com/zhangyang.com" target="_blank">
<img class="heardImg" src="/developer/dongfeng.jpg" style="border-radius: 9999px;">
<p class="nameP">
东风
</p>
</a>
</div>

<div class="developer">
<a href="https://gitee.com/sh1yu" target="_blank">
<img class="heardImg" src="/developer/sh1yu.png" style="border-radius: 9999px;">
<p class="nameP">
sh1yu
</p>
</a>
</div>

</div> 



## wind 有话说
如果你能看到这里，说明你确实把官网整个浏览了一遍，在这里先对你表示感谢，感谢你对于我们的支持！  
作为一名开源新人，很荣幸能得到大家的关注和贡献，其实项目的成长速度已经超乎了我的想象，有人说是我找对了方向，也有人说是组织的支持。  
其实我做这个项目的目的很简单，因为我烦他！没错，曾经短信就是我的噩梦！  
有名人说过：**懒惰是人类进步的阶梯** _（我说的，万一以后成名人了呢😂）_
最初的预想也很简单，那就是把全部的短信揉吧揉吧放一块，也没考虑什么可用性，什么多厂商，什么兼容性，反正就一个字：简单 _（没错，这是一个字）_
之后就匆匆发布了第一版，但是确实没想到成长速度会这么快，也成功得到了Dromara各位前辈大神的支持，这让我有了把这个项目完善起来，做大的想法，于是在
上述贡献者的建议和组织各位前辈大神的指导下，我做了未来的规划，甚至于可以说，等到2.X版本走到尽头3.X发布的时候，他会跟现在大不一样，可用性，易用性都会得到
很大的提升，当然文档的更新也不会落下，保姆级文档必须标配。  
扯了一大堆，也没啥营养，诸位就当在学（mo）习（yu）了。期望大家多多支持吧，开始的目的是让简单的事情回归简单，以后的目的就是让懒人变的更懒。总之，我们会不断的成长，不断的前行
如果大家在使用中有问题或者建议，记得一定要告诉我们，不怕诸位问题多，一定会为诸位尽早解答。也希望大家关注下我的公众号，后续有计划在公众号中放置一些实用的东西（尤其对于小白），当然这些需要时间的沉淀。  
最后，感谢大家听我废话，祝愿各位：**永不加班，永不脱发，睡觉睡到自然醒，数钱数到数学不够用（手不会抽筋的，啥年代了，还带什么现金）**  
看了这么多废话，肯定累了，赶紧点个 [star](https://gitee.com/dromara/sms4j)缓解一下😎
