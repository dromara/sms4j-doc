---
title: 🦜敬请期待
index: false
icon: play
category:
  - 前言
---
<h4 align="center" style="margin: 0 0 0; font-weight: bold;">SMS4J -- 让发送短信变的更简单</h4>
<h4 align="center" style="margin: 0 0 0; font-weight: bold;"><img width="130" align="center" src="/logo.png"></h4>

<h4 align="center" style="margin: 0 0 0; font-weight: bold;">
<a align="center" href="https://gitee.com/dromara/sms4j/stargazers" ><img src="https://gitee.com/the-wind-is-like-a-song/sms_aggregation/badge/star.svg?theme=gvp"></a>
<a align="center" href="https://gitee.com/dromara/sms4j/master/LICENSE" style="padding-left: 5px"><img src="https://img.shields.io/badge/license-Apache--2.0-green"></a>
<a align="center" href="https://gitee.com/dromara/sms4j" style="padding-left: 5px"><img src="https://img.shields.io/badge/version-v3.0.0-blue"></a>
</h4>




在日常的开发过程中，短信的发送经常使用（尤其是中小型的外包公司），毕竟不是每个公司都有阿里腾讯一样的实力，
也不是每个都像银行联通等公司一样有内部的短信规程。第三方的短信往往是最常见的解决方案，但是市面上第三方短信服务商众多，
各家都有不同的方式和标准，每次需要使用时候，都需要花费时间去阅读文档和编写相应的工具，为一个短信浪费了太多的精力和时间。
sms4j的初衷是为了简化短信，邮件等功能所占用的时间，最大程度的减少开发的重复代码和文档的阅读时间，同时还能实现动态的配置切换，
失败重试，等功能。
  
**如果我们为您带来了方便，或者您觉得还算值得鼓励，请用您发财的小手帮助点上一个start**  
  
[gitee](https://gitee.com/dromara/sms4j)  
[github](https://github.com/fengruge/sms_aggregation)  
[Doc 文档](https://apidoc.gitee.com/dromara/sms4j/)

## 🎁支持厂商一览
_兼容不断增加中_
- **合一短信**
- **云片短信**
- **天翼云短信**
- **网易云短信**
- **阿里云国内短信**
- **腾讯云国内短信**
- **华为云国内短信**
- **京东云国内短信**
- **容联云国内短信**
- **亿美软通国内短信**
