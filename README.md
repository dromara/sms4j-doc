
## 
<h4 align="center" style="margin: 0 0 0; font-weight: bold;">
<a align="center" href="https://gitee.com/dromara/sms4j/stargazers" ><img src="https://gitee.com/the-wind-is-like-a-song/sms_aggregation/badge/star.svg?theme=gvp"></a>
<a align="center" href="https://gitee.com/dromara/sms4j/master/LICENSE" style="padding-left: 5px"><img src="https://img.shields.io/badge/license-Apache--2.0-green"></a>
<a align="center" href="https://gitee.com/dromara/sms4j" style="padding-left: 5px"><img src="https://img.shields.io/badge/version-v2.0.1-blue"></a>
</h4>

## 🎗️特性
<div id="ela">
<div id="peculiarityId">
<p class="peculiarity">
✅ 开箱即用，简单方便
</p>
<p class="peculiarity">
✅ 兼容SpringBoot3，升级无障碍  
</p>
<p class="peculiarity">
✅ 支持多厂商配置，轻松便捷
</p>
<p class="peculiarity">
✅ 支持异步短信延迟短信等诸多额外功能
</p>
<p class="peculiarity">
✅ 使用方便，新手友好
</p>
<p class="peculiarity">
✅ 最小化依赖，最大化降低依赖冲突风险
</p>
<p class="peculiarity">
✅ 配置灵活，支持多种配置方式
</p>
</div>
<div>
<img src="docs/src/.vuepress/public/logo.png">
</div>
</div>


## 📀maven安装
   ```xml
    <dependency>
        <groupId>org.dromara.sms4j</groupId>
        <artifactId>sms4j-spring-boot-starter</artifactId>
        <version> version </version>
    </dependency>
   ```
## 🛠️基础配置
   ```yaml
    sms:
       alibaba:
         #阿里云的accessKey
         accessKeyId: 您的accessKey
         #阿里云的accessKeySecret
         accessKeySecret: 您的accessKeySecret
         #短信签名
         signature: 测试签名
         #模板ID 用于发送固定模板短信使用
         templateId: SMS_215125134
         #模板变量 上述模板的变量
         templateName: code
         #请求地址 默认为dysmsapi.aliyuncs.com 如无特殊改变可以不用设置
         requestUrl: dysmsapi.aliyuncs.com
       huawei:
         #华为短信appKey
         appKey: 5N6fvXXXX920HaWhVXXXXXX7fYa
         #华为短信appSecret
         app-secret: Wujt7EYzZTBXXXXXXEhSP6XXXX
         #短信签名
         signature: 华为短信测试
         #通道号
         sender: 8823040504797
         #模板ID 如果使用自定义模板发送方法可不设定
         template-id: acXXXXXXXXc274b2a8263479b954c1ab5
         #华为回调地址，如不需要可不设置或为空
         statusCallBack:
          #华为分配的app请求地址
         url: https://XXXXX.cn-north-4.XXXXXXXX.com:443
   ```
## 🧿使用
```java
@RestController
@RequestMapping("/test/")
public class DemoController {

    // 测试发送固定模板短信
    @RequestMapping("/")
    public void test() {
         //阿里云向此手机号发送短信
        SmsFactory.createSmsBlend(SupplierType.ALIBABA).sendMessage("18888888888","123456");
        //华为短信向此手机号发送短信
        SmsFactory.createSmsBlend(SupplierType.HUAWEI).sendMessage("16666666666","000000");
    }
}
```

## 💾代码托管

[![dromara/SMSAggregation](https://gitee.com/dromara/sms_aggregation/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/dromara/sms_aggregation)